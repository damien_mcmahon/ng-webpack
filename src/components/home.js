import HomeController from '../controllers/home';

const homeTemplate = `
  <div class="home-component--wrapper">
    <email-list-item email="email" ng-repeat="email in $ctrl.emails.collection.items track by $index"></email-list-item>
  </div>
`;

const HomeComponent = {
  template: homeTemplate,
  controller: HomeController,
  bindings: {
    emails: '<'
  }
}

export default HomeComponent;
