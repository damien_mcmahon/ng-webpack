import angular from 'angular';
import HomeComponent from './home';
import EmailListComponent from './email-list-item';
import EmailComponent from './email';

const AppComponents = angular.module('app.components', []);

AppComponents.component('home', HomeComponent);
AppComponents.component('email', EmailComponent);
AppComponents.component('emailListItem', EmailListComponent);

export default AppComponents;
