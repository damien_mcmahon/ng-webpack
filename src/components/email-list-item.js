const emailListTemplate = `
  <div class="email-list-item--wrapper">
    <h1 class="email-list-item--title">{{::$ctrl.email.name}}</h1>
    <p class="email-list-item--subjects">{{::$ctrl.email.subjects[0]}}</p>
    <a class="email-list-item--link" ui-sref="email({id:$ctrl.email.id})">More</a>
  </div>
`;

const EmailListComponent = {
  template: emailListTemplate,
  bindings: {
    email: '<'
  }
};

export default EmailListComponent;
