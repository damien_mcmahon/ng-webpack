import EmailController from '../controllers/email.js';

const emailViewTemplate = `
  <div class="email-view--wrapper">
    <div class="input--wrapper">
      <label class="input--label">Message Name:</label>
      <p class="input--text email--title">{{$ctrl.email.name}}</p>
    </div>
    <div class="input--wrapper">
      <label class="input--label">Subject Line:</label>
      <p class="input--text email--subject-line">{{::$ctrl.email.subjects[0]}}</p>
    </div>
    <div class="email--body-wrapper">
      <div class="email--body-actions-wrapper">
        <button 
          class="email--html-view" 
          ng-class="{'active': htmlViewActive}"
          ng-click="setVisibleText('html')">HTML</button>
        <button 
          class="email--plain-view"
          ng-class="{'active': plainViewActive}"
          ng-click="setVisibleText('plain')">Plain text</button>
      </div>
      <email-viewer message="emailBody" ng-if="htmlViewActive"></email-viewer>
      <textarea 
        class="email--body" 
        disabled="disabled" 
        ng-if="plainViewActive">
          {{emailBody}}
      </textarea>
    </div>
  </div>
`;

const EmailComponent = {
  template: emailViewTemplate,
  controller: EmailController,
  bindings: {
    email: '<'
  }
}

export default EmailComponent;
