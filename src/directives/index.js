import angular from 'angular';
import EmailViewer from './email-viewer';

const AppDirectives = angular.module('app.directives', []);

AppDirectives.directive('emailViewer', () => new EmailViewer());

export default AppDirectives;

