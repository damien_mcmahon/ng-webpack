class EmailViewer {
  constructor() {
    this.template = `
    <div class="email-viewer--wrapper">
      <iframe class="email-viewer--iframe"/>
    </div>`;
    this.scope = {
      message: '<'
    }
  }

  link(scope, element) {
    const messageContents = scope.message;
    const appendChild = element[0].querySelector('.email-viewer--iframe');
    appendChild.srcdoc = scope.message;
  };
}

export default EmailViewer;
