class APIService {
  constructor($http, urlConstants) {
    this.$http = $http;
    this.urlConstants = urlConstants;
  }

  getEmailList() {
    return this.$http({
      method: 'GET',
      url: `${this.urlConstants.BASE_URL}${this.urlConstants.DATA_PATH}emails.json`
    }).then(res => res.data)
  }

  getEmail(emailId) {
    return this.$http({
      method: 'GET',
      url: `${this.urlConstants.BASE_URL}${this.urlConstants.DATA_PATH}email-${emailId}.json`
    }).then(res => res.data);
  }
};

APIService.$inject = ['$http', 'urlConstants'];

export default APIService;
