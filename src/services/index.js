import angular from 'angular';
import APIService from './api-service';

const AppServices = angular.module('app.services', []);
AppServices.service('APIService', APIService);

export default AppServices;
