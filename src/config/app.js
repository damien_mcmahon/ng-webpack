const AppConfig = ($stateProvider, $urlRouterProvider) => {
  $stateProvider.state({
    name: 'home',
    url: '/',
    component: 'home',
    resolve: {
      emails: (APIService) => APIService.getEmailList()
    }
  })
  .state({
    name: 'email',
    url: '/email/:id',
    component: 'email',
    resolve: {
      email: (APIService, $transition$) => APIService.getEmail($transition$.params().id) 
    }
  })
  $urlRouterProvider.otherwise('/');
};

AppConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

export default AppConfig;
