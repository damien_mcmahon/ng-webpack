export default class EmailController {
  constructor($scope) {
    this.scope = $scope;
    this.HTML_VIEW = 'html';
    this.scope.setVisibleText = this.setVisibleText.bind(this);
  }

  $onInit() {
    this.scope.htmlViewActive = false;
    this.scope.plainViewActive = true;
  }

  $onChanges(changes) {
    if(changes.email.currentValue) {
      this.scope.emailBody = this.setBodyText(changes.email.currentValue);
    }
  }

  setBodyText(email) {
    return this.scope.htmlViewActive ? email.body.html : email.body.text;
  }

  setVisibleText(view) {
   if(view === this.HTML_VIEW) {
     this.scope.htmlViewActive = true;
     this.scope.plainViewActive = false;
     this.scope.emailBody = this.setBodyText(this.email);
   } else {
     this.scope.htmlViewActive = false;
     this.scope.plainViewActive = true;
     this.scope.emailBody = this.setBodyText(this.email);
   } 
  }
}
EmailController.$inject = ['$scope'];
