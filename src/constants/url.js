const urlConstants = {
  BASE_URL: '/',
  DATA_PATH: 'data/' 
};

export default urlConstants;
