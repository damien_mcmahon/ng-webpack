import angular from 'angular';
import urlConstants from './url';

const AppConstants = angular.module('app.constants', []);
AppConstants.constant('urlConstants', urlConstants);

export default AppConstants;
