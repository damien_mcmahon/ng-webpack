import angular from 'angular';
import uiRouter from 'angular-ui-router';
import appConfig from './config/app';
import AppComponents from './components';
import AppServices from './services';
import AppConstants from './constants';
import AppDirectives from './directives';

import css from '../styles/main.less';

const rootElement = document.getElementById('app');
const appDependencies = [
  uiRouter,
  AppComponents.name,
  AppServices.name,
  AppConstants.name,
  AppDirectives.name
];

angular.module('emailList', appDependencies)
  .config(appConfig);

angular.bootstrap(rootElement, ['emailList']);
