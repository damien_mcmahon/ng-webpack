var path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './src/app.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname,'dist')
  },
  devtool: "cheap-eval-source-map",
  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader'
    },{
      test: /\.json/,
      exclude: /node_modules/,
      loader: 'url-loader?name=data/[name].[ext]'
    }],
    rules: [{
      test: /\.less$/,
      use: [
        'style-loader',
        { loader: 'css-loader', options: { importLoaders: 1 } },
        'less-loader'
      ]
    }]
  },
  plugins: [
    new ExtractTextPlugin('app.bundle.css'),
    new HtmlWebpackPlugin({
        template: './index.html',
        minify: { collapseWhitespace: true }
    })
  ]
};
