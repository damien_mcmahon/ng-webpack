describe('Service: APIService', function() {
  var testJSON, httpBackend, APIService;

  beforeEach(function() {
    module('emailList');

    testJSON = {
      collection: [{name: 'email-1'}]
    }

    inject(function(_$httpBackend_, _APIService_, _urlConstants_){
      httpBackend = _$httpBackend_; 
      APIService = _APIService_;
    });
  });

  //NOTE - Just happy path testing...
  it('returns a list of json from the given endpoint', function()
  {
    var responseData = { data: testJSON};
    var retrievedList = APIService.getEmailList();
    httpBackend.expectGET('/data/emails.json').respond(200, responseData);
    httpBackend.flush();
    expect(retrievedList).toEqual(testJSON);
  });

  it('returns the email data for a given message', function()
  {
    var testMessage = {name: 'A message', subject: 'A subject'};
    var retrievedEmail = APIService.getEmail(1);
    httpBackend.expectGET('/data/emails-1.json').respond(200, testMessage);
    httpBackend.flush();
    expect(retrievedEmail).toEqual(testMessage);
  });
});
