# NG Webpack #
This is a test of using Angular, Webpack, ES6 and karma.

To get things working, run `npm install` or `yarn` if you like emojis (who doesn't?) 👌

## Running Dev Server ##
Once your dependencies are installed then type `npm start` and open `http://localhost:8080` to see the magic.

## Running a Build ##
If you want to build for production run `npm run build`. The required assets are then ready to be pushed to a static page server like
`surge.sh`.

## Running tests ##
Running tests is as simple as `npm run test`. 
**NOT WORKING** - Karma+ES6+Webpack isn't playing nice 😣

# Todo: #
- [x] Write the app using data-down approach.
- [x] Get and display a list of emails.
- [x] Get and display a plain text or html email.
- [x] Deployable assets.
- [ ] Write unit tests.
- [ ] Write simple E2E with `protractor` using page objects.
