module.exports = function(config) {
  config.set({
    browsers: ['PhantomJS'],
    frameworks: ['jasmine'],

    files: [
      'node_modules/angular/angular.js',
      'node_modules/angular-mocks/angular-mocks.js',
      'app/src/**/*.js',
      'test/unit/**/*.spec.js'
    ],
    preprocessors: {
      'app/src/**/*.js': ['webpack'],
      'test/unit/**/*.spec.js': ['webpack']
    },
    webpack: {
      module: {
        loaders: [{
          test: /\.js/,
          exclude: /node_modules/,
          loader: 'babel-loader'
        }]
      },
      watch: true
    }
  })
};
